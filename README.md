# Spring_security
## This project is to understand the spring security with jwt token. 
- [ ] Java 17
- [ ] Gradle for dependency management
- [ ] Postgres for Database
- [ ] MyBatis for ORM
- [ ] Liquibase for database migration

## Features
- [ ] Login user
- [ ] Register user
- [ ] Fetch user
- [ ] Update user
- [ ] Delete user