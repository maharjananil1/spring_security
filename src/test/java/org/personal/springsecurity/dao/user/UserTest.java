package org.personal.springsecurity.dao.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author maharjananil @Date 04/17/2023
 */
public class UserTest {
  @Test
  void createUserWithNoArgsTest() {
    User user = new User();
    Assertions.assertNotNull(user);
    validateUserNull(user);
  }

  @Test
  void createUserWithNoArgsSetterTest() {
    User user = new User();
    user.setId("id");
    user.setEmail("email");
    user.setFirstName("firstname");
    user.setLastName("lastname");
    user.setPassword("password");
    validateUserNotNull(user);
  }

  @Test
  void createUserWithAllArgsTest() {
    User user = new User("Id", "firstName", "lastName", "password", "email","role");
    validateUserNotNull(user);
  }

  @Test
  void createUserWithBuilderTest() {
    User user =
        User.builder()
            .id("id")
            .email("email")
            .firstName("firstname")
            .lastName("lastname")
            .password("password")
            .build();
    validateUserNotNull(user);
  }

  private static void validateUserNotNull(User user) {
    Assertions.assertNotNull(user);
    Assertions.assertNotNull(user.getId());
    Assertions.assertNotNull(user.getFirstName());
    Assertions.assertNotNull(user.getLastName());
    Assertions.assertNotNull(user.getPassword());
    Assertions.assertNotNull(user.getEmail());
  }

  private static void validateUserNull(User user) {
    Assertions.assertNull(user.getId());
    Assertions.assertNull(user.getFirstName());
    Assertions.assertNull(user.getLastName());
    Assertions.assertNull(user.getPassword());
    Assertions.assertNull(user.getEmail());
  }
}
