package org.personal.springsecurity.dao.user;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Optional;

/**
 * @author maharjananil @Date 04/17/2023
 */
@Mapper
public interface UserDao {
  List<User> findAll();

  Optional<User> findById(String id);
  Optional<User> findByEmail(String email);

  void insert(User user);

  void update(User student, int id);

  void delete(String id);
}
