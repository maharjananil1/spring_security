package org.personal.springsecurity.dao.user;
/**
 * @author maharjananil @Date 04/18/2023
 */
public enum Role {
    ADMIN,
    USER
}
