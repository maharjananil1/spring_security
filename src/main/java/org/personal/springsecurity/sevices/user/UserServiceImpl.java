package org.personal.springsecurity.sevices.user;

import lombok.RequiredArgsConstructor;
import org.personal.springsecurity.dao.user.Role;
import org.personal.springsecurity.dao.user.User;
import org.personal.springsecurity.dao.user.UserDao;
import org.personal.springsecurity.domains.dto.UserDTO;
import org.personal.springsecurity.domains.response.UserAddResponse;
import org.personal.springsecurity.exceptions.ApiRequestException;
import org.personal.springsecurity.exceptions.user.UserExceptions;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * @author maharjananil @Date 04/17/2023
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
  private final UserDao userDao;
  private final PasswordEncoder passwordEncoder;


  @Override
  public UserAddResponse insert(UserDTO userDTO) {
    User user =
        User.builder()
            .id(UUID.randomUUID().toString().replace("-", ""))
            .firstName(userDTO.getFirstName())
            .lastName(userDTO.getLastName())
            .email(userDTO.getEmail())
            .password(passwordEncoder.encode(userDTO.getPassword()))
            .role(Role.ADMIN.name())
            .build();
    userDao.insert(user);
    return new UserAddResponse(user.getId());
  }

  @Override
  public List<User> findAll() {
    return this.userDao.findAll();
  }

  @Override
  public User findById(String id) {
    return this.userDao
        .findById(id)
        .orElseThrow(
            () ->
                new ApiRequestException(
                    UserExceptions.USER_NOT_FOUND.getCode(),
                    UserExceptions.USER_NOT_FOUND.getMessage()));
  }
}
