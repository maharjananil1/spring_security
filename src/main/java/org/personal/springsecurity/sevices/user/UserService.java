package org.personal.springsecurity.sevices.user;

import org.personal.springsecurity.dao.user.User;
import org.personal.springsecurity.domains.dto.UserDTO;
import org.personal.springsecurity.domains.response.UserAddResponse;

import java.util.List;

/**
 * @author maharjananil @Date 04/17/2023
 */
public interface UserService {
  UserAddResponse insert(UserDTO user);

  List<User> findAll();

  User findById(String id);
}
