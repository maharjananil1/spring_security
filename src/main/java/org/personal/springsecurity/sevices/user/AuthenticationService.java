package org.personal.springsecurity.sevices.user;

import lombok.RequiredArgsConstructor;
import org.personal.springsecurity.configs.JwtService;
import org.personal.springsecurity.dao.user.UserDao;
import org.personal.springsecurity.domains.request.LoginRequest;
import org.personal.springsecurity.domains.response.AuthenticationResponse;
import org.personal.springsecurity.exceptions.ApiRequestException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author maharjananil @Date 04/18/2023
 */
@Service
@RequiredArgsConstructor
public class AuthenticationService {
  private final UserDao userDao;
  private final JwtService jwtService;
  private final AuthenticationManager authenticationManager;

  public AuthenticationResponse authenticate(LoginRequest request) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
    var user =
        this.userDao
            .findByEmail(request.getUsername())
            .orElseThrow(() -> new ApiRequestException("AUTH001", "User not found"));
    var token = this.jwtService.generateToken(user);
    return new AuthenticationResponse(token);
  }
}
