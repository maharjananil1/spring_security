package org.personal.springsecurity.configs;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.personal.springsecurity.dao.user.UserDao;
import org.personal.springsecurity.exceptions.ApiRequestException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author maharjananil @Date 04/18/2023
 */
@Configuration
@RequiredArgsConstructor
public class AuthenticationConfig {
  private final UserDao userDao;

  @Bean
  public UserDetailsService userDetailsService() {
    return username ->
        userDao
            .findByEmail(username)
            .orElseThrow(() -> new ApiRequestException("AUTH001", "User not found"));
  }

  @Bean
  public AuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
    authenticationProvider.setUserDetailsService(this.userDetailsService());
    authenticationProvider.setPasswordEncoder(this.passwordEncoder());
    return authenticationProvider;
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration)
      throws Exception {
    return configuration.getAuthenticationManager();
  }
}
