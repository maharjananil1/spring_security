package org.personal.springsecurity.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * @author maharjananil @Date 04/17/2023
 */
@Getter
@AllArgsConstructor
public class ApiException {
    private final String message;
    private final long timestamp;
    private final HttpStatus httpStatus;
    private final String errorCode;
}
