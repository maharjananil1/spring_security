package org.personal.springsecurity.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author maharjananil @Date 04/17/2023
 */
@ControllerAdvice
public class ApiExceptionHandler {
  @ExceptionHandler(value = {ApiRequestException.class})
  public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    ApiException apiException =
        new ApiException(e.getMessage(), System.currentTimeMillis(), status, e.getErrorCode());
    return new ResponseEntity<>(apiException, status);
  }
}
