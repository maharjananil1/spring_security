package org.personal.springsecurity.exceptions;

import lombok.*;
import org.springframework.stereotype.Component;

/**
 * @author maharjananil @Date 04/17/2023
 */
@Component
@Getter
public class ApiRequestException extends RuntimeException {
  private String errorCode;

  public ApiRequestException() {
  }

  public ApiRequestException(String errorCode,String message) {
    super(message);
    this.errorCode = errorCode;
  }

  public ApiRequestException(String message, Throwable cause) {
    super(message, cause);
  }

  public ApiRequestException(Throwable cause) {
    super(cause);
  }

  public ApiRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
