package org.personal.springsecurity.exceptions.user;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author maharjananil @Date 04/17/2023
 */
@Getter
@AllArgsConstructor
public enum UserExceptions {
  USER_NOT_FOUND("USR001", "User not found");
  private final String code;
  private final String message;
}
