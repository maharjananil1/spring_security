package org.personal.springsecurity.controllers;

import lombok.RequiredArgsConstructor;
import org.personal.springsecurity.dao.user.User;
import org.personal.springsecurity.domains.dto.UserDTO;
import org.personal.springsecurity.domains.response.UserAddResponse;
import org.personal.springsecurity.sevices.user.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author maharjananil @Date 04/17/2023
 */
@RestController
@RequestMapping("user")
@RequiredArgsConstructor
public class UserController {
  private final UserService service;

  @PostMapping
  public ResponseEntity<UserAddResponse> save(@RequestBody UserDTO userDTO) {
    return ResponseEntity.ok(this.service.insert(userDTO));
  }

  @GetMapping("/all")
  public ResponseEntity<List<User>> fetchAll() {
    return ResponseEntity.ok(this.service.findAll());
  }

  @GetMapping("{id}")
  public ResponseEntity<User> fetchById(@PathVariable("id") String id) {
    return ResponseEntity.ok(this.service.findById(id));
  }
}
