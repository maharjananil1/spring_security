package org.personal.springsecurity.controllers;

import lombok.RequiredArgsConstructor;
import org.personal.springsecurity.domains.request.LoginRequest;
import org.personal.springsecurity.domains.response.AuthenticationResponse;
import org.personal.springsecurity.sevices.user.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author maharjananil @Date 04/18/2023
 */
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
  private final AuthenticationService service;

  @PostMapping("/authenticate")
  public ResponseEntity<AuthenticationResponse> authenticate(
      @RequestBody LoginRequest loginRequest) {
    AuthenticationResponse authenticate = this.service.authenticate(loginRequest);
    return ResponseEntity.ok(authenticate);
  }
}
