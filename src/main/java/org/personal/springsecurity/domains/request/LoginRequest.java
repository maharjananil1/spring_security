package org.personal.springsecurity.domains.request;

import lombok.Data;

/**
 * @author maharjananil @Date 04/18/2023
 */
@Data
public class LoginRequest {
    private String username;
    private String password;
}
