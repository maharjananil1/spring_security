package org.personal.springsecurity.domains.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author maharjananil @Date 04/18/2023
 */
public class UserAddResponse extends AbstractAddResponse{

    public UserAddResponse(String id) {
        super(id);
    }
}
