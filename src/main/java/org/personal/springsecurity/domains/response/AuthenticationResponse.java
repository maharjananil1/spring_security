package org.personal.springsecurity.domains.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author maharjananil @Date 04/18/2023
 */
@Data
@AllArgsConstructor
public class AuthenticationResponse {
    private String token;
}
