package org.personal.springsecurity.domains.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author maharjananil @Date 04/18/2023
 */
@Data
@RequiredArgsConstructor
public abstract class AbstractAddResponse {
    private final String id;
}
