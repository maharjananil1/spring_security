package org.personal.springsecurity.domains.dto;

import jakarta.annotation.Nonnull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.personal.springsecurity.dao.user.Role;

/**
 * @author maharjananil @Date 04/17/2023
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
  private String firstName;
  private String lastName;
  private String email;
  private String password;
  private Role role;
}
